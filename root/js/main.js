$(function () {

 // ドロワーメニュー
  $('.drawer').drawer();

// スクロールダウンボタン
  // $('.scroll-box').on('click',function(){
  //       // 特定の要素を超えた
  //       $("html,body").animate({scrollTop:$('#h1-top').offset().top-55},1000,'easeOutCirc');
  //   });

// スクロールダウンをスクロールしたら消す
  $(window).on("scroll",function(){
    // 100px移動したら
    if ($(this).scrollTop()>50) {
      $('.scroll-box,.catch-box').stop().animate({opacity:0},300);
      }
    else{
      $('.scroll-box,.catch-box').stop().animate({opacity:1},300);
      }
    });

  // CSSメソッド版
  // $(window).on("scroll",function(){
  // if ($(window).scrollTop()>200) {
  //   $('.scroll-box').css("display","none");
  //   }
  // else{
  //   $('.scroll-box').css("display","block");
  //   }
  // });

// catch-boxの高さの指定
    $(document).ready(function(){
    //画面の高さを取得して、変数wHに代入
    var wH = $(window).height(); 
    //div#exampleの高さを取得を取得して、変数divHに代入
    var divH = $('.catch-box').innerHeight();
    // ボックス要素より画面サイズが大きければ実行
    if(wH > divH){
      // div#exampleに高さを加える
      $('.catch-box').css('height',wH+'px'); 
    }
});

// スムーズスクロール
    /*
     * Back-toTop button (Smooth scroll)
     */
    $('#gotop').on('click', function () {

        // Smooth Scroll プラグインを実行
        $.smoothScroll({
            easing: 'easeOutExpo', // イージングの種類
            speed: 2000             // 所要時間
        });
    });

// 画像のリサイズ
  // $('.nailthumb-container').nailthumb({width:150,height:150,method:'resize',fitDirection:'top left'});

// inview

// h1のアニメーション
  $('.h1-mvleft').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
    if(isInView){
      $(this).stop().addClass('mvleft');
    }
  });

// 文字色変更
  $('.color-change').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
    if(isInView){
      $(this).stop().addClass('mdn');
    }
  });



// h2のアニメーション
  $('.h2-mvdown').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
    if(isInView){
      $(this).stop().addClass('mvh2');
    }
  });

  $('.h2-mvup').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
    if(isInView){
      $(this).stop().addClass('mvh2');
    }
  });

  $('.h2-mvhr').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
    if(isInView){
      $(this).stop().addClass('mvh2');
    }
  });

// カードのアニメーション
  // $('.card-mvup').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
  //   if(isInView){
  //     $(this).stop().addClass('mvup');
  //   }
  // });

// ボタンのアニメーション
  $('.btn-mvfi').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
    if(isInView){
      $(this).stop().addClass('mvfi');
    }
  });

// 遅れて出てくる？
$('.cards').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
    if(isInView){
      $(this).find('.card-mvup').each(function(i) {
        $(this).delay(150 * i).queue(function() {
          $(this).stop().addClass('mvup').dequeue();
        });
      });
    };
  });

// 制作フローのアニメーション
  $('.goal-mvfi').on('inview', function(event, isInView, visiblePartX, visiblePartY) {
    if(isInView){
      $(this).stop().addClass('mvfi');
    }
  });

// ドロワーメニュー
    $(document).ready(function() {
      $('.drawer').drawer();
    });

// ライトボックス
    $('[data-fancybox]').fancybox();

// ローディング画面
    Pace.on('done', function(){
        $('.wrapper').fadeIn();
    });



});

